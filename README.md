# Bicara Plugin - Schedule

Bicara Schedule Plugin enables scheduling future TOPS sessions by integrating with a shared Google calendar.

- `scheduleNextSession`: Create a new event on the shared Google Calendar with the session information captured by the Schedule Plugin dashboard
- `sendReminders`: A Cron job running every 30 minutes to check the reminder queue and send reminders to the session participants if needed

More information about the Google Calendar integration with Bicara can be found under [Bicara Core Step 3](https://gitlab.com/action-lab-aus/bicara/zoomsense-tops/-/blob/main/docs/4_QuickStart_Google_Calendar.md).

## Local Development

Once the `.runtimeconfig.json` file is prepared under the `/functions` folder (configuration details can be found under [Bicara Core Step 6](https://gitlab.com/action-lab-aus/bicara/zoomsense-tops/-/blob/main/docs/7_QuickStart_Functions.md)), Firebase Functions can be tested locally using the command:

```
npm run emulators
```

This ensures that the emulator data will be imported when Firebase Emulator starts and exported when the session ends.
